#include <math>
#include <float>
#include "irrlicht.h"

int _RTLENTRY _EXPFUNC _matherr(struct _exception* e)
{
   e->retval=1.0;
   return 1;
}

int _RTLENTRY _EXPFUNC _matherrl(struct _exception* e)
{
   e->retval=1.0;
   return 1;
}

#ifdef createDevice
#error createDevice defined!
#endif

#if defined(_STDCALL_SUPPORTED)
#define IRRCALLCONV __stdcall  // Declare the calling convention.
#else
#define IRRCALLCONV
#endif // STDCALL_SUPPORTED
namespace irr
{
/* createDevice() function hijack
 * The purpose of this is to disable
 * floating point exception handling for borland
 * before actually going into the real createDevice function()
 */
extern "C"
{
IRRLICHT_API IrrlichtDevice* IRRCALLCONV REAL_createDevice(video::E_DRIVER_TYPE driverType,
										  const core::dimension2d<s32>& windowSize,
										  u32 bits, bool fullscreen,
										  bool stenticbuffer, bool vsync, IEventReceiver* res,
										  const wchar_t* version);

IRRLICHT_API IrrlichtDevice* IRRCALLCONV createDevice(video::E_DRIVER_TYPE driverType,
										  const core::dimension2d<s32>& windowSize,
										  u32 bits, bool fullscreen,
										  bool stenticbuffer, bool vsync, IEventReceiver* res,
										  const wchar_t* version)
{
_control87(MCW_EM, MCW_EM);     //disable BC exception handling
    return REAL_createDevice(driverType, windowSize,
                             bits, fullscreen, stenticbuffer,
                             vsync, res, version);
}
}//extern "C"
}
