#========================================================================
#  Borland Makefile -- created using AutoBake 1.08
#
#  Project:     Irrlicht
#  TimeStamp:   Thu Mar 17 00:14:18 2005
#========================================================================

#main Macro Definitions
# Build Types:
#   DebugBuild   -- Debug Compile
#   ReleaseBuild -- Release Compile
# Project Types:
#   Con -- Console,          Win -- Windows
#   DLL -- Dynamic Link Lib, SLIB -- Static Lib
# Compiling Options:
#   RT -- dynamic runtime,   MT -- multi-threaded
#   WIDE -- Unicode/Wide
DLL = 1
MT =1
ReleaseBuild = 1

#Project Properties
Project     = Irrlicht
ProjectPath = .
BuildOutput = Irrlicht
ProjCFlags  = -w- -I".\include" -D_MSC_VER=1100;NO_STRICT;_WIN32;WIN32;NDEBUG;_WINDOWS;IRRLICHT_EXPORTS
ProjLFlags  = -t
ProjTFlags  =
ProjRFlags  =
ProjLib     =

gui_impl = \
    CGUIButton.obj CGUICheckbox.obj CGUIComboBox.obj \
    CGUIContextMenu.obj CGUIEditBox.obj \
    CGUIEnvironment.obj \
    CGUIFileOpenDialog.obj CGUIFont.obj \
    CGUIImage.obj CGUIInOutFader.obj \
    CGUIListBox.obj CGUIMenu.obj \
    CGUIMeshViewer.obj CGUIMessageBox.obj \
    CGUIModalScreen.obj \
    CGUIScrollBar.obj CGUISkin.obj CGUIStaticText.obj CGUITabControl.obj \
    CGUIToolBar.obj \
    CGUIWindow.obj

video impl = \
    CVideoModeList.obj \
    CSoftwareTexture.obj \
    CTRFlat.obj CTRFlatWire.obj \
    CTRGouraud.obj CTRGouraudWire.obj \
    CTRTextureFlat.obj CTRTextureFlatWire.obj \
    CTRTextureGouraud.obj CTRTextureGouraudNoZ.obj CTRTextureGouraudWire.obj \
    CVideoSoftware.obj \
    CZBuffer.obj \
    COpenGLNormalMapRenderer.obj \
    COpenGLShaderMaterialRenderer.obj \
    COpenGLTexture.obj CVideoOpenGL.obj \
    CD3D8NormalMapRenderer.obj \
    CD3D8ShaderMaterialRenderer.obj \
    CDirectX8Texture.obj CVideoDirectX8.obj \
    CColorConverter.obj \
    CFPSCounter.obj \
    CImage.obj \
    CImageLoaderBmp.obj \
    CImageLoaderJPG.obj CImageLoaderPCX.obj \
    CImageLoaderPSD.obj CImageLoaderTGA.obj \
    CVideoNull.obj \
    CD3D9HLSLMaterialRenderer.obj CD3D9NormalMapRenderer.obj \
    CD3D9ShaderMaterialRenderer.obj \
    CDirectX9Texture.obj CVideoDirectX9.obj

scene_impl = \
    CGeometryCreator.obj \
    CMeshManipulator.obj \
    CSceneManager.obj \
    C3DSMeshFileLoader.obj \
    CAnimatedMeshMD2.obj CAnimatedMeshMS3D.obj \
    CCSMLoader.obj \
    CDefaultMeshFormatLoader.obj \
    CLMTSMeshFileLoader.obj CMY3DMeshFileLoader.obj \
    COCTLoader.obj \
    CQ3LevelMesh.obj \
    CStaticMeshOBJ.obj \
    CXAnimationPlayer.obj \
    CXFileReader.obj \
    CXMeshFileLoader.obj \
    CSceneNodeAnimatorCollisionResponse.obj CSceneNodeAnimatorDelete.obj \
    CSceneNodeAnimatorFlyCircle.obj CSceneNodeAnimatorFlyStraight.obj \
    CSceneNodeAnimatorFollowSpline.obj CSceneNodeAnimatorRotation.obj \
    CSceneNodeAnimatorTexture.obj \
    CAnimatedMeshSceneNode.obj CBillboardSceneNode.obj \
    CCameraFPSSceneNode.obj CCameraMayaSceneNode.obj \
    CCameraSceneNode.obj \
    CDummyTransformationSceneNode.obj \
    CEmptySceneNode.obj CLightSceneNode.obj \
    CMeshSceneNode.obj COctTreeSceneNode.obj \
    CShadowVolumeSceneNode.obj CSkyBoxSceneNode.obj \
    CTerrainSceneNode.obj \
    CTestSceneNode.obj CTextSceneNode.obj \
    CWaterSurfaceSceneNode.obj \
    CParticleBoxEmitter.obj CParticleFadeOutAffector.obj \
    CParticleGravityAffector.obj CParticlePointEmitter.obj \
    CParticleSystemSceneNode.obj \
    CMetaTriangleSelector.obj COctTreeTriangleSelector.obj \
    CSceneCollisionManager.obj \
    CTerrainTriangleSelector.obj \
    CTriangleBBSelector.obj CTriangleSelector.obj

io_impl = \
    CFileList.obj CFileSystem.obj \
    CLimitReadFile.obj CMemoryReadFile.obj \
    CReadFile.obj CTextReader.obj CWriteFile.obj \
    CXMLReader.obj CXMLWriter.obj \
    CZipReader.obj

irr_impl = \
    CIrrDeviceLinux.obj CIrrDeviceStub.obj CIrrDeviceWin32.obj \
    CLogger.obj \
    COSOperator.obj CStringParameters.obj \
    Irrlicht.obj \
    os.obj

zlib = \
    adler32.obj crc32.obj \
    inffast.obj inflate.obj \
    inftrees.obj trees.obj \
    uncompr.obj zutil.obj

jpeglib = \
    cdjpeg.obj \
    jcapimin.obj jcapistd.obj \
    jccoefct.obj jccolor.obj \
    jcdctmgr.obj jchuff.obj \
    jcinit.obj jcmainct.obj \
    jcmarker.obj jcmaster.obj \
    jcomapi.obj jcparam.obj \
    jcphuff.obj jcprepct.obj \
    jcsample.obj jctrans.obj \
    jdapimin.obj jdapistd.obj \
    jdatadst.obj jdatasrc.obj \
    jdcoefct.obj jdcolor.obj \
    jddctmgr.obj jdhuff.obj \
    jdinput.obj jdmainct.obj \
    jdmarker.obj \
    jdmaster.obj \
    jdmerge.obj \
    jdphuff.obj \
    jdpostct.obj \
    jdsample.obj \
    jdtrans.obj \
    jerror.obj \
    jfdctflt.obj jfdctfst.obj \
    jfdctint.obj jidctflt.obj \
    jidctfst.obj jidctint.obj \
    jidctred.obj \
    jmemmgr.obj jmemnobs.obj \
    jquant1.obj jquant2.obj \
    jutils.obj \
    rdbmp.obj rdcolmap.obj \
    rdgif.obj rdppm.obj \
    rdrle.obj rdswitch.obj \
    rdtarga.obj \
    transupp.obj \
    wrbmp.obj wrgif.obj \
    wrppm.obj wrrle.obj \
    wrtarga.obj

OBJ = $(gui_impl) $(video impl) $(scene_impl) $(io_impl) $(irr_impl) $(zlib) $(jpeglib) bcbfix.obj

RES =

#Borland Makefile Directives
.autodepend
.nosilent
.path.obj = .\Bin
.path.cpp = .\source;.\source\zlib;.\source\jpeglib
.path.c   = $(.path.cpp)
.path.i   = $(.path.obj)
.path.exe = $(.path.obj)
.path.dll = $(.path.obj)
.path.lib = $(.path.obj)
.path.asm = $(.path.obj)
.path.res = $(.path.obj)

#Borland C++ Tools
CC   = BCC32
RC   = BRCC32
PREP = CPP32
LINK = ILINK32
ILIB = IMPLIB
TLIB = TLIB

#Borland Startup
BCC32RTLIB = import32.lib cw32
CFlags = -c @$(MAKEDIR)\bcc32.cfg
!if $d(Con)
BCC32OBJ = c0x32
TFlags = -tWC
LFlags = -Gn -c -ap -Tpe
BuildOutput = $(BuildOutput).exe
!elif $d(Win)
BCC32OBJ = c0w32
TFlags = -tW
LFlags = -Gn -c -aa -Tpe
BuildOutput = $(BuildOutput).exe
!elif $d(DLL)
BCC32OBJ = c0d32
TFlags = -tWD
LFlags = -Gn -c -aa -Tpd
BuildOutput = $(BuildOutput).dll
!elif $d(SLIB)
BCC32OBJ =
TFlags =
LFlags =
BuildOutput = $(BuildOutput).lib
!else
!error No Project Type set
!endif
#Multi-threaded & Runtime Linkage
!if $d(MT) && $d(RT)
CFlags = $(CFlags) -tWM $(TFlags) $(TFlags)R
BCC32RTLIB = $(BCC32RTLIB)mti
!elif $d(MT)
CFlags = $(CFlags) -tWM $(TFlags)
BCC32RTLIB = $(BCC32RTLIB)mt
!elif $d(RT)
CFlags = $(CFlags) $(TFlags) $(TFlags)R
BCC32RTLIB = $(BCC32RTLIB)i
!else
CFlags = $(CFlags) $(TFlags)
!endif
#Unicode/Wide
!ifdef WIDE
BCC32OBJ = $(BCC32OBJ)w.obj
CFlags = $(CFlags) -tWU
!else
BCC32OBJ = $(BCC32OBJ).obj
!endif
#Release Build & Debug Build
!if $d(ReleaseBuild)
CFlags = -v- $(CFlags)
LFlags = -v- $(LFlags)
!elif $d(DebugBuild)
CFlags = -v -y -Od $(CFlags)
LFlags = -v $(LFlags)
!endif


#Dependency Rules
BuildProject : MakeBinPath $(BuildOutput) $(PostEvents)

BuildASM : MakeBinPath $(OBJ:.obj=.asm)

Preprocess : MakeBinPath $(OBJ:.obj=.i)

!ifndef SLIB
$(BuildOutput) : $(OBJ) $(RES)
	$(LINK) @&&| -c +
	$(LFlags) $(ProjLFlags) -L$(.path.obj);$(Global_Lib) +
	$(BCC32OBJ) +
	$(OBJ), +
	$<, , +
	$(BCC32RTLIB) $(ProjLib), , +
	$(RES) @$(MAKEDIR)\ilink32.cfg
|
!ifdef DLL
	$(ILIB) $*.lib $@
	&$(TLIB) "$*.lib", "$*.lst"
	@type $*.lst|more
	@del $*.lst
!endif

!elif $d(SLIB)
$(BuildOutput) : $(ProjLib) $(OBJ)
	&$(TLIB) $(ProjTFlags) "$@" /a $**, $*.lst
	@type $*.lst|more
	@del $*.lst
!endif

MakeBinPath :
	@echo on
	@if not exist $(.path.obj) (md $(.path.obj))

.cpp.i :
	$(PREP) $(CFlags) $(ProjCFlags) -D$(Global_Define) -I$(Global_Include) -o"$@" "$**"
	@echo.

.cpp.asm :
	$(CC) -S $(CFlags) $(ProjCFlags) -D$(Global_Define) -I$(Global_Include) -o"$@" "$**"
	@echo.

.cpp.obj :
	$(CC) $(CFlags) $(ProjCFlags) -D$(Global_Define) -I$(Global_Include) -o"$@" "$**"
	@echo.

.c.i :
	$(PREP) $(CFlags) $(ProjCFlags) -D$(Global_Define) -I$(Global_Include) -o"$@" "$**"
	@echo.

.c.asm :
	$(CC) -S $(CFlags) $(ProjCFlags) -D$(Global_Define) -I$(Global_Include) -o"$@" "$**"
	@echo.

.c.obj :
	$(CC) $(CFlags) $(ProjCFlags) -D$(Global_Define) -I$(Global_Include) -o"$@" "$**"
	@echo.

.rc.res :
	$(RC) $(RFlags) -r $(ProjRFlags) -fo"$@" "$**"
	@echo.

CleanAll : Clean $(CleanAPIEvents)
	@del /S /q $(BuildOutput)
	@del /S /q $(Project).bak
!ifdef DLL
	@del /S /q $(Project).lib
!endif
	@cd ..
	@rd Bin

Clean :
	@cd $(.path.obj)
	@del /S /q $(gui_impl)
	@del /S /q $(video impl)
	@del /S /q $(scene_impl)
	@del /S /q $(io_impl)
	@del /S /q $(irr_impl)
	@del /S /q $(zlib)
	@del /S /q $(jpeglib)
	@del /S /q bcbfix.obj $(RES)
	@del /S /q $(gui_impl:.obj=.i)
	@del /S /q $(video impl:.obj=.i)
	@del /S /q $(scene_impl:.obj=.i)
	@del /S /q $(io_impl:.obj=.i)
	@del /S /q $(irr_impl:.obj=.i)
	@del /S /q $(zlib:.obj=.i)
	@del /S /q $(jpeglib:.obj=.i)
	@del /S /q $(Project).csm
	@del /S /q $(Project).il* $(Project).map
	@del /S /q $(Project).tds $(Project).tr2
