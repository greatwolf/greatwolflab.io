
#ifdef __BORLANDC__
#ifndef _BCBFIX_H
#define _BCBFIX_H

/*************************************************
 * Extra defines to help detect OS Version
 * since winnt.h header in bcb 5.5.1 are
 * missing them
 *************************************************/
#define VER_SUITE_PERSONAL              0x00000200
#define VER_SUITE_BLADE                 0x00000400
#define VER_SUITE_EMBEDDED_RESTRICTED   0x00000800
#define VER_SUITE_SECURITY_APPLIANCE    0x00001000

#define VER_NT_WORKSTATION              0x0000001
#define VER_NT_DOMAIN_CONTROLLER        0x0000002
#define VER_NT_SERVER                   0x0000003

//Borland math funcs
#define sqrtf (float)sqrt
#define fmodf (float)fmod

//Miscellenous stuff to help w/ compilation
#define false 0
#define _chdir(a) chdir(a)

/* Rename the real createDevice() so that
 * my own createDevice() gets called.
 * This is needed because borland exception handling *MUST*
 * be disabled before createDevice is actually executed. Otherwise
 * application will crash. My createDevice disables that and then calls
 * the real createDevice.
 */
#ifdef __C_IRR_DEVICE_WIN32_H_INCLUDED__
#define createDevice REAL_createDevice
#endif

#endif //_BCBFIX_H
#endif //__BORLANDC__
