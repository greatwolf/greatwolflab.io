#SDL Preset
!ifdef SDL

SDL_BasePath = F:\OSS\SDL12
SDL_Define =
SDL_IncludePath = $(SDL_BasePath)\include
SDL_LibPath = $(SDL_BasePath)\lib
SDL_LibFiles = SDL.lib SDLmain.lib

Global_Define = $(Global_Define);$(SDL_Define)
Global_Include = $(Global_Include);$(SDL_IncludePath)
Global_Lib = $(Global_Lib);$(SDL_LibPath)
ProjLib = $(ProjLib) $(SDL_LibFiles)

SDL_RUNTIME = $(SDL_LibPath)\SDL.DLL

PostEvents = $(PostEvents) SDLRuntime
CleanAPIEvents = $(CleanAPIEvents) CleanSDL

SDLRuntime : $(SDL_RUNTIME)
    &@copy /V /Y $** $(.path.obj)

CleanSDL : $(SDL_RUNTIME)
    &@del /S /q $(**F)

!endif
