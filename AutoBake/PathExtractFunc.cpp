#include <ctype>
#include <cstring>
#include "PathExtractFunc.h"

using namespace std;

const char* ExtractExt(const char *arg)
{
    //Note: !ext != ""
    const char *ext = strrchr(arg, '.');

    if(!ext)
        return "";

    return ext;
}//ExtractExt

const char* ExtractFile(const char *arg)
{
    /* ensure argCopy exist after function exits so
     * caller's accessing valid memory.
     * Note argCopy is resetted each time between calls
     * so make sure caller does whatever it needs before
     * calling this function again.
     */
    static char argCopy[256] = "";
    const char *file = strrchr(arg, '\\');
    const char *ext = file ? ExtractExt(file):ExtractExt(arg);

    //arg has no path
    if(!file)
        file = arg;

    //start working from the char after backslash
    else if(strlen(file) > 1)
        file++;

    if( isalnum(file[0]) )
        if(strlen(ext) > 0)
        {
            strncpy(argCopy, file, ext - file);
            argCopy[ext - file] = '\0';
        }//if
        else strcpy(argCopy, file);

    return argCopy;
}//ExtractFile

const char* ExtractDir(const char *arg)
{
    /* ensure argCopy exist after function exits so
     * caller's accessing valid memory.
     * Note argCopy is resetted each time between calls
     * so make sure caller does whatever it needs before
     * calling this function again.
     */
    static char argCopy[256] = ".";
    const char *dir = strrchr(arg, '\\');

    if(dir && dir - arg > 0)
    {
        strncpy(argCopy, arg, dir - arg);
        argCopy[dir - arg] = '\0';
    }//if

    return argCopy;
}//ExtractDir
