/**
 * Autobake -- makefile generator for Borland's make.exe
 *
 * Autobake generates a compatible Borland makefile from
 * either a SciTE session file or a single .cpp source.
 * The generated makefile is placed in the directory where
 * the ses|cpp file resides.
 *
 * Rewritten yet a third time. The older C-style library IO
 * is used to keep overall executable size smaller and faster.
 *
 * The generated makefile relies on bcc32proj.mak's dependency
 * rules and macro definitions.
 *
 * by Greatwolf
 */

/*
 * Changelog:
 *  1.08, 1-9-05
 *   - added ProjTFlags variable for holding tlib options/switches
 *   - added $(Project).bak to the list of files for cleanup
 *  1.07, 12-15-04
 *   - added .path.c and .path.i directive for c source & preprocessed source.
 */

#include <cstdio>
#include <cstring>
#include <vector>
#include <ctime>
#include <assert>
#include "PathExtractFunc.h"

using namespace std;
const float VERSION = 1.08f;
const unsigned short MAXSIZE_STR = 256;

//give files global scope for easier access
FILE *session  = NULL;
FILE *makefile = NULL;
vector<long> cpplistpos;
long pathcpp = 0;
enum
{
    SESSIONARG,
    CPPARG
}argtype = SESSIONARG;

typedef void (*pfunc)(char *str);
void ScanSession();
void WriteMak(const int argc, char *const argv[]);
void EachPosDo(pfunc action);
void Cpp2Obj(char *str);
void FillcppPath(char *filepath);

int main(int argc, char *argv[])
{
    char strmakefile[MAXSIZE_STR] = "";

    //print Autobake title
    printf("Autobake %2.2f by Greatwolf\n", VERSION);

    //verify session|cpp|c argument
    if(argc < 2 ||
       (strcmp(ExtractExt(argv[1]), ".ses") &&
        strcmp(ExtractExt(argv[1]), ".cpp") &&
        strcmp(ExtractExt(argv[1]), ".c") ) )
    {
        printf("  usage: Autobake session|cpp|c [macro definitions]\n");
        exit(1);
    }//if
    if(!strcmp(ExtractExt(argv[1]), ".ses") )
        argtype = SESSIONARG;
    else argtype = CPPARG;

    printf("  Opening \"%s\" ...\n", argv[1]);
    if(argtype == SESSIONARG)
    {
        session = fopen(argv[1], "rb");
        if(!session)
        {
            printf("\tfailed\n");
            exit(1);
        }//if

        //find and save all .cpp position from session into cpplistpos
        ScanSession();
    }//if

    strcat(strmakefile, argv[1]);
    strcat(strmakefile, ".mak");
    printf("  Making \"%s\" ...\n", strmakefile);
    makefile = fopen(strmakefile, "w+b");
    if(!makefile)
    {
        printf("\tfailed\n");
        exit(1);
    }//if

    WriteMak(argc, argv);

    //close all files
    fclose(session);
    fclose(makefile);
    return 0;
}//main

void ScanSession()
{
    char buf[MAXSIZE_STR] = "";
    long lastline;

    assert(!ferror(session));
    fseek(session, 0, SEEK_SET);
    lastline = ftell(session);
    fscanf(session, "%s", buf);
    while(!feof(session))
    {
        if(!strcmp(ExtractExt(buf), ".cpp") || //handle both .cpp
           !strcmp(ExtractExt(buf), ".c") )    //and .c source
        {
            printf("    %s\n", buf);
            cpplistpos.push_back(lastline);
        }//if
        lastline = ftell(session);
        fscanf(session, "%s", buf);
    }//while
}//ScanSession

void WriteMak(const int argc, char *const argv[])
{
    time_t stamp = time(NULL);

    //assert(!ferror(session));
    assert(!ferror(makefile));
    assert(argc > 1);

    fprintf(makefile,
            "#========================================================================\n"
            "#  Borland Makefile -- created using AutoBake %2.2f\n"
            "#  \n"
            "#  Project:     %s\n"
            "#  TimeStamp:   %s"
            "#========================================================================\n",
            VERSION, ExtractFile(argv[1]), ctime(&stamp) );
    fprintf(makefile, "\n"
                      "#%s Macro Definitions\n"
                      "# Build Types:\n"
                      "#   DebugBuild   -- Debug Compile\n"
                      "#   ReleaseBuild -- Release Compile\n"
                      "# Project Types:\n"
                      "#   Con -- Console,          Win -- Windows\n"
                      "#   DLL -- Dynamic Link Lib, SLIB -- Static Lib\n"
                      "# Compiling Options:\n"
                      "#   RT -- dynamic runtime,   MT -- multi-threaded\n"
                      "#   WIDE -- Unicode/Wide\n"
                      "# API & Framework presets:\n"
                      "#   SDL -- SDL presets,      wxWindows -- wxWindows presets\n"
                    , ExtractFile(argv[1]) );
    for(int i = 2; i < argc; i++)
    {
        printf("    %s = 1\n", argv[i]);
        fprintf(makefile, "%s = 1\n", argv[i]);
    }//for
    fprintf(makefile, "\n"
                      "#Project Properties\n"
                      "Project     = %s\n", ExtractFile(argv[1]) );
    fprintf(makefile, "ProjectPath = %s\n", ExtractDir(argv[1]) );
    fprintf(makefile, "BuildOutput = %s\n"
                      "ProjCFlags  = \n"
                      "ProjLFlags  = \n"
                      "ProjTFlags  = \n"
                      "ProjRFlags  = \n"
                      "ProjLib     = \n"
                      "\n"
                      "OBJ = ",
                      ExtractFile(argv[1]) );
    if(argtype == CPPARG)
        Cpp2Obj(argv[1]);
    else EachPosDo(&Cpp2Obj);
    fprintf(makefile, "\n\n"
                      "RES = ");

    fprintf(makefile, "\n\n"
                      "#Borland Makefile Directives\n"
                      ".autodepend\n"
                      ".nosilent\n"
                      ".path.obj = %s\\Bin\n", ExtractDir(argv[1]) );

    //save file pointer at .path.cpp line
    //displace 12-bytes starting from ".path.cpp = "
    pathcpp = ftell(makefile) + 12;
    fprintf(makefile, ".path.cpp = %s", ExtractDir(argv[1]) );
    EachPosDo(&FillcppPath);

    fprintf(makefile, "\n"
                      ".path.c   = $(.path.cpp)\n"
                      ".path.i   = $(.path.obj)\n"
                      ".path.exe = $(.path.obj)\n"
                      ".path.dll = $(.path.obj)\n"
                      ".path.lib = $(.path.obj)\n"
                      ".path.asm = $(.path.obj)\n"
                      ".path.res = $(.path.obj)\n");
    fprintf(makefile, "\n"
                      "!include $(MAKEDIR)\\..\\bcc32proj.mak\n");
}//WriteMak

void EachPosDo(pfunc action)
{
    char buf[MAXSIZE_STR] = "";
    for(vector<long>::iterator i = cpplistpos.begin(); i != cpplistpos.end(); i++)
    {
        //set file pointer and check for success
        if(fseek(session, *i, SEEK_SET) )
        {
            printf("    error reading from session\n");
            exit(1);
        }//if
        //read in cpp filename and do action
        fscanf(session, "%s", buf);
        action(buf);
    }//for
}//EachPosDo

void Cpp2Obj(char *str)
{
    //write cpp filename in makefile
    //but with .obj extension instead
    printf("    %s.obj\n", ExtractFile(str) );
    fprintf(makefile, "\\\n"
                      "\t%s.obj",
                      ExtractFile(str) );
}//Cpp2Obj

void FillcppPath(char *filepath)
{
    long lastline = ftell(makefile);
    char buf[MAXSIZE_STR] = "";
    char *pathpos = NULL;
    int pathlenth = strlen(ExtractDir(filepath) );

    //reposition pointer to pathcpp and read in path
    fseek(makefile, pathcpp, SEEK_SET);
    fscanf(makefile, "%s", buf);
    //set pointer back to original position
    //must be done before writing to makefile again
    fseek(makefile, lastline, SEEK_SET);

    pathpos = strstr(buf, ExtractDir(filepath) );
    if(!pathpos || pathpos[pathlenth] == '\\')
    {
        printf("    %s\n", ExtractDir(filepath) );
        fprintf(makefile, ";%s",
                          ExtractDir(filepath) );
    }//if
}//FillcppPath
