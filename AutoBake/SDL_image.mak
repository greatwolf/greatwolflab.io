#SDL Preset
!ifdef SDL_image

SDL_image_BasePath = F:\OSS\SDL_image
SDL_image_Define =
SDL_image_IncludePath = $(SDL_image_BasePath)
SDL_image_LibPath = $(SDL_image_BasePath)
SDL_image_LibFiles = IMG.lib

Global_Define = $(Global_Define);$(SDL_image_Define)
Global_Include = $(Global_Include);$(SDL_image_IncludePath)
Global_Lib = $(Global_Lib);$(SDL_image_LibPath)
ProjLib = $(ProjLib) $(SDL_image_LibFiles)

#~ SDL_image_RUNTIME = $(SDL_image_LibPath)\SDL_image.DLL

PostEvents = $(PostEvents) SDL_image_Runtime
CleanAPIEvents = $(CleanAPIEvents) CleanSDL_image

SDL_image_Runtime : $(SDL_image_RUNTIME)
    &@copy /V /Y $** $(.path.obj)

CleanSDL_image : $(SDL_image_RUNTIME)
    &@del /S /q $(**F)

!endif
