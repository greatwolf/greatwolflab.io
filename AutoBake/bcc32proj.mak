#Borland C++ Tools
CC   = BCC32
RC   = BRCC32
PREP = CPP32
LINK = ILINK32
ILIB = IMPLIB
TLIB = TLIB

#Borland Startup
BCC32RTLIB = import32.lib cw32
CFlags = -c @$(MAKEDIR)\bcc32.cfg
!if $d(Con)
BCC32OBJ = c0x32
TFlags = -tWC
LFlags = -Gn -c -ap -Tpe
BuildOutput = $(BuildOutput).exe
!elif $d(Win)
BCC32OBJ = c0w32
TFlags = -tW
LFlags = -Gn -c -aa -Tpe
BuildOutput = $(BuildOutput).exe
!elif $d(DLL)
BCC32OBJ = c0d32
TFlags = -tWD
LFlags = -Gn -c -aa -Tpd
BuildOutput = $(BuildOutput).dll
!elif $d(SLIB)
BCC32OBJ =
TFlags =
LFlags =
BuildOutput = $(BuildOutput).lib
!else
!error No Project Type set
!endif
#Multi-threaded & Runtime Linkage
!if $d(MT) && $d(RT)
CFlags = $(CFlags) -tWM $(TFlags) $(TFlags)R
BCC32RTLIB = $(BCC32RTLIB)mti
!elif $d(MT)
CFlags = $(CFlags) -tWM $(TFlags)
BCC32RTLIB = $(BCC32RTLIB)mt
!elif $d(RT)
CFlags = $(CFlags) $(TFlags) $(TFlags)R
BCC32RTLIB = $(BCC32RTLIB)i
!else
CFlags = $(CFlags) $(TFlags)
!endif
#Unicode/Wide
!ifdef WIDE
BCC32OBJ = $(BCC32OBJ)w.obj
CFlags = $(CFlags) -tWU
!else
BCC32OBJ = $(BCC32OBJ).obj
!endif
#Release Build & Debug Build
!if $d(ReleaseBuild)
CFlags = -v- $(CFlags)
LFlags = -v- $(LFlags)
!elif $d(DebugBuild)
CFlags = -v -y -Od $(CFlags)
LFlags = -v $(LFlags)
!endif

#wxWindows Macro Template
!ifdef wxWindows
!ifndef RT
ProjCFlags = $(ProjCFlags) @$(WXWIN)\src\msw\wxw32.cfg
ProjLib = $(ProjLib) wx24s_bcc.lib
!else
ProjCFlags = $(ProjCFlags) -DWXUSINGDLL=1 @$(WXWIN)\src\msw\wxw32.cfg
ProjLib = $(ProjLib) wx24_bcc.lib
!endif
!endif


#Pull in 3rd API settings
!include $(MAKEDIR)\..\presets.mak

#Dependency Rules
BuildProject : MakeBinPath $(BuildOutput) $(PostEvents)

BuildASM : MakeBinPath $(OBJ:.obj=.asm)

Preprocess : MakeBinPath $(OBJ:.obj=.i)

!ifndef SLIB
$(BuildOutput) : $(OBJ) $(RES)
	$(LINK) @&&| -c +
	$(LFlags) $(ProjLFlags) -L$(.path.obj);$(Global_Lib) +
	$(BCC32OBJ) +
	$(OBJ), +
	$<, , +
	$(BCC32RTLIB) $(ProjLib), , +
	$(RES) @$(MAKEDIR)\ilink32.cfg
|
!ifdef DLL
	$(ILIB) $*.lib $@
	&$(TLIB) "$*.lib", "$*.lst"
	@type $*.lst|more
	@del $*.lst
!endif

!elif $d(SLIB)
$(BuildOutput) : $(ProjLib) $(OBJ)
	&$(TLIB) $(ProjTFlags) "$@" /a $**, $*.lst
	@type $*.lst|more
	@del $*.lst
!endif

MakeBinPath :
	@echo on
	@if not exist $(.path.obj) (md $(.path.obj))

.cpp.i :
	$(PREP) $(CFlags) $(ProjCFlags) -D$(Global_Define) -I$(Global_Include) -o"$@" "$**"
	@echo.

.cpp.asm :
	$(CC) -S $(CFlags) $(ProjCFlags) -D$(Global_Define) -I$(Global_Include) -o"$@" "$**"
	@echo.

.cpp.obj :
	$(CC) $(CFlags) $(ProjCFlags) -D$(Global_Define) -I$(Global_Include) -o"$@" "$**"
	@echo.

.c.i :
	$(PREP) $(CFlags) $(ProjCFlags) -D$(Global_Define) -I$(Global_Include) -o"$@" "$**"
	@echo.

.c.asm :
	$(CC) -S $(CFlags) $(ProjCFlags) -D$(Global_Define) -I$(Global_Include) -o"$@" "$**"
	@echo.

.c.obj :
	$(CC) $(CFlags) $(ProjCFlags) -D$(Global_Define) -I$(Global_Include) -o"$@" "$**"
	@echo.

.rc.res :
	$(RC) $(RFlags) -r $(ProjRFlags) -fo"$@" "$**"
	@echo.

CleanAll : Clean $(CleanAPIEvents)
	@del /S /q $(BuildOutput)
	@del /S /q $(Project).bak
!ifdef DLL
	@del /S /q $(Project).lib
!endif
	@cd ..
	@rd Bin

Clean :
	@cd $(.path.obj)
	@del /S /q $(OBJ) $(RES)
	@del /S /q $(OBJ:.obj=.i)
	@del /S /q $(OBJ:.obj=.asm)
	@del /S /q $(Project).csm
	@del /S /q $(Project).il* $(Project).map
	@del /S /q $(Project).tds $(Project).tr2

Debug :
!if !$d(DLL) && !$d(SLIB)
	start /max TD32 -t$(ProjectPath) -l- $(.path.obj)\$(BuildOutput) $(args)
!endif

Go :
!if !$d(DLL) && !$d(SLIB)
	@cd $(.path.obj)
	$(BuildOutput) $(args)
	@echo.
!endif
