#ifndef PATHEXTRACTFUNC_H
#define PATHEXTRACTFUNC_H

//Path Extraction functions
const char* ExtractExt  (const char *arg);
const char* ExtractFile (const char *arg);
const char* ExtractDir  (const char *arg);

#endif
