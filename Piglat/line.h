#ifndef LINE_H_
#define LINE_H_
#include <vector>
#include "Word.h"

class Page;
template <typename resultType>
class AnalysisObj;

class Line
{
    public:
        Line(Page *parent) : myparent(parent)
        {}
        ~Line()
        {myparent = NULL;}
        Line(const Line &rhs) : myparent(rhs.myparent), mywords(rhs.mywords) {}
        void Build();
        void Convert();
        void Display();
        int TotalWords() const;
        int CountLeadingVowels() const;
        int CountNonleadingVowels() const;

    private:
        std::vector<Word> mywords;
        Page *myparent;

        template <typename resultType>
        resultType Analyze(const Word &singleword, AnalysisObj<resultType> &runthis);
};

#endif
