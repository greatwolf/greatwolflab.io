#include <ostream>
#include <cstring>
#include <assert>
#include "apstring.h"

using namespace std;

unsigned int apstring::ctorcount = 0;
unsigned int apstring::dtorcount = 0;

apstring::apstring(const char rhs) : mylength(1), mycap(mylength + 1)
{
    mystring = new char[mycap];
    mystring[0] = rhs;
    mystring[1] = '\0';
    ctorcount++;
}

apstring::apstring(const char *rhs) : mylength(strlen(rhs)), mycap(mylength + 1)
{
    mystring = new char[mycap];
    strcpy(mystring, rhs);
    ctorcount++;
}

apstring::apstring(const apstring &rhs) : mylength(rhs.length()), mycap(mylength + 1)
{
    mystring = new char[mycap];
    strcpy(mystring, rhs.c_string());
    ctorcount++;
}

apstring::~apstring()
{
    delete[] mystring;
    mystring = NULL;
    dtorcount++;
}

const char* apstring::operator = (const char *rhs)
{
    mylength = strlen(rhs);

    if(mylength + 1 > mycap)
        resize(mylength + 1);

    std::strcpy(mystring, rhs);
    return rhs;
}

const apstring& apstring::operator = (const apstring &rhs)
{
    if(this == &rhs)
        return rhs;

    mylength = rhs.length();

    if(mylength + 1 > mycap)
    {
        resize(mylength + 1);
    }//if

    strcpy(mystring, rhs.mystring);
    return rhs;
}

const apstring& apstring::operator += (const char rhs)
{
    if(mylength + 1 == mycap)
        resize(mylength + 2);

    mystring[mylength++] = rhs;
    mystring[mylength] = '\0';

    return *this;
}

const apstring& apstring::operator += (const apstring &rhs)
{
    const apstring tmp = rhs;

    mylength += tmp.mylength;
    if(mylength + 1 > mycap)
        resize(mylength + 1);

    assert(mycap >= mylength + 1);
    strcat(mystring, tmp.mystring);

    return *this;
}

char& apstring::operator [] (const unsigned int pos)
{
    if(BoundOut(pos))
    {
        cerr << "Out of Bound string index!" << endl;
        exit(1);
    }//if
    return mystring[pos];
}

const char& apstring::operator [] (const unsigned int pos) const
{
    if(BoundOut(pos))
    {
        cerr << "Out of Bound string index!" << endl;
        exit(1);
    }//if
    return mystring[pos];
}

int apstring::find(const char key) const
{
    int pos = npos;
    for(unsigned int i = 0; i < mylength; i++)
    {
        if(mystring[i] != key)
            continue;

        pos = i;
        break;
    }//for

    return pos;
}

int apstring::length() const
{
    return mylength;
}

const char* apstring::c_string() const
{
    return mystring;
}

const apstring operator + (const apstring &lhs, const apstring &rhs)
{
    apstring tmp = lhs;

    return tmp += rhs;
}

const apstring operator + (const apstring &lhs, const char rhs)
{
    apstring tmp = lhs;

    return tmp += rhs;
}

bool operator == (const apstring &lhs, const apstring &rhs)
{
    return !(lhs != rhs);
}

bool operator != (const apstring &lhs, const apstring &rhs)
{
    return strcmp(lhs.c_string(), rhs.c_string());
}

std::ostream& operator << (std::ostream &os, const apstring &rhs)
{
    os << rhs.mystring;
    return os;
}

std::istream& operator >> (std::istream &is, apstring &rhs)
{
    char buf;
    bool havestuff = is >> buf;

    if(havestuff)
        rhs = "";

    while(havestuff)
    {
        rhs += buf;
        buf = is.get();
        if(buf == ' ' || buf == '\t' || buf == '\n' || is.eof())
        {
            havestuff = false;
            is.putback(buf);
        }//if
    }//while

    return is;
}

void getline(std::istream &is, apstring &target)
{
    char buf = is.get();

    //kill first whitespace
    if(buf == ' ' || buf == '\t' || buf == '\n')
        buf = is.get();

    target = "";

    while(buf != '\n' && !is.eof())
    {
        target += buf;
        buf = is.get();
    }//while
}

bool apstring::BoundOut(const int index) const
{
    return (0 > index || index >= mylength);
}

void apstring::resize(const int newsize)
{
    assert(newsize > 0);
    mycap = newsize;
    const char *tmp = mystring;
    mystring = new char[mycap];

    if(mylength + 1 > mycap)
    {
        strncpy(mystring, tmp, mycap - 1);
        mystring[mycap - 1] = '\0';
    }
    else strncpy(mystring, tmp, mylength);

    delete[] tmp;
}
