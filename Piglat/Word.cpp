#include "Word.h"

extern const Word vowels = "aeiouAEIOU",
                  consonants = "bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ";

//~ bool Word::VowelFirst() const
//~ {
    //~ return (vowels.find((*this)[0]) != apstring::npos);
//~ }

bool Word::HasVowel() const
{
    for(int i = 0; i < this->length(); i++)
        if(vowels.find((*this)[i]) != apstring::npos)
            return true;
    return false;
}
