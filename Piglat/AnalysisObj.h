#ifndef ANALYSISOBJ_H_
#define ANALYSISOBJ_H_

template <typename resultType>
class AnalysisObj
{
    public:
        virtual void operator () (const char letter) = 0;
        virtual resultType result() = 0;
        virtual bool finished() const = 0;
        virtual resultType handleEmptyWord() = 0;
        virtual void resetState() = 0;
};

#endif
