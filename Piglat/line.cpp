#include <assert>
#include "page.h"
#include "line.h"
#include "PigAnalysis.h"

using namespace std;

void Line::Build()
{
    assert(myparent->infile.is_open());
    Word currentword;

    do
    {
        myparent->infile >> currentword;
        if(currentword.length())
            mywords.push_back(currentword);
        while(myparent->infile.peek() == ' ' ||
              myparent->infile.peek() == '\t')
            myparent->infile.ignore();
    }while(!myparent->infile.eof() && myparent->infile.peek() != '\n');
}

void Line::Convert()
{
    PigAnalysis translatePig;
    for(vector<Word>::iterator cur = mywords.begin(); cur != mywords.end(); cur++)
    {
        translatePig.resetState();
        *cur = Analyze<PigAnalysis::ResultType>(*cur, translatePig);
    }//for
}

void Line::Display()
{
    for(vector<Word>::iterator cur = mywords.begin(); cur != mywords.end(); cur++)
        cout << *cur << ' ';
    cout << endl;
}

int Line::TotalWords() const
{
    return mywords.size();
}

int Line::CountLeadingVowels() const
{
    unsigned short count = 0;

    for(vector<Word>::const_iterator cur = mywords.begin(); cur != mywords.end(); cur++)
    {
        if(cur->VowelFirst())
            count++;
    }//for

    return count;
}

int Line::CountNonleadingVowels() const
{
    unsigned short count = 0;

    for(vector<Word>::const_iterator cur = mywords.begin(); cur != mywords.end(); cur++)
    {
        if(!cur->VowelFirst() && cur->HasVowel())
            count++;
    }//for

    return count;
}

template <typename resultType>
resultType Line::Analyze(const Word &singleword, AnalysisObj<resultType> &runthis)
{
    //handle an empty word
    if(!singleword.length())
        return runthis.handleEmptyWord();

    resultType compoundresult;
    for(int i = 0; i < singleword.length(); i++)
    {
        if(runthis.finished())
        {
            compoundresult += runthis.result();
            runthis.resetState();
        }
        runthis(singleword[i]);
    }

    return compoundresult += runthis.result();
}
