#include "PigAnalysis.h"

void PigAnalysis::operator () (const char letter)
{
    //handle nonalpha case first
    if(consonants.find(letter) == apstring::npos &&
       vowels.find(letter) == apstring::npos)
    {
        nonalpha += letter;
        imdone = true;
        return;
    }//else

    if(vowels.find(letter) != apstring::npos)
        foundvowel = true;

    if(foundvowel)
        modifiedWord += letter;
    else if(consonants.find(letter) != apstring::npos)
        consonant += letter;
}

Word PigAnalysis::result()
{
    if(foundvowel)
        modifiedWord += '-' + consonant + "ay" + nonalpha;
    else if(isYword())
        return consonant + nonalpha;
    else modifiedWord = consonant + nonalpha;

    return modifiedWord;
}

bool PigAnalysis::finished() const
{
    return imdone;
}

Word PigAnalysis::handleEmptyWord()
{
    return "";
}

void PigAnalysis::resetState()
{
    /*
    foundvowel = false;
    imdone = false;
    modifiedWord = "";
    consonant = "";
    nonalpha = "";
    /*/
    this->~PigAnalysis();
    new (this)PigAnalysis();
    //*/
}

bool PigAnalysis::isYword() const
{
    if(!foundvowel && consonant.length())
        return consonant[consonant.length() - 1] == 'y';
    return false;
}
