#include <iostream>
#include <assert>
#include "Piglat.h"
#include "page.h"


using namespace std;

int main(int argc, char *argv[])
{
    cout << "Piglat converter\n" << endl;
    if(argc < 2)
    {
        ShowUsage();
        return 1;
    }//if

    Page page;

    page.Build(argv[1]);

    //get the stats first before converting
    //since words will be changed after converting
    unsigned int leadingvows = page.TotalFirstVowels();
    unsigned int nonleadingvows = page.TotalNonleadingVowels();

    page.Convert();
    page.Display();

    cout << "Total lines done: " << page.TotalLines() << endl
         << "Total words done: " << page.TotalWords() << endl
         << "Total words with leading vowels: " << leadingvows << endl
         << "Total words with nonleading vowels: " << nonleadingvows << endl;
}//main

void ShowUsage()
{
    cout << "  usage: exe [inputfile]" << endl;
}//ShowUsage
