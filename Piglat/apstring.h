#ifndef APSTRING_H_
#define APSTRING_H_
#include <iostream>

class apstring
{
public:
    static const int npos = -1;
    apstring(const char rhs);
    apstring(const char *rhs = "");
    apstring(const apstring &rhs);
    ~apstring();

    const char* operator = (const char *rhs);
    const apstring& operator = (const apstring &rhs);

    const apstring& operator += (const char rhs);
    const apstring& operator += (const apstring &rhs);

    char& operator [] (const unsigned int pos);
    const char& operator [] (const unsigned int pos) const;

    int find(const char key) const;
    inline int length() const;
    inline const char* c_string() const;

friend std::ostream& operator << (std::ostream &os, const apstring &rhs);
friend std::istream& operator >> (std::istream &is, apstring &rhs);

    static unsigned int ctorcount, dtorcount;
private:
    char *mystring;
    unsigned int mylength;
    unsigned int mycap;

    bool BoundOut(const int index) const;
    void resize(const int newsize);
};//apstring

const apstring operator + (const apstring &lhs, const apstring &rhs);
const apstring operator + (const apstring &lhs, const char rhs);
inline bool operator == (const apstring &lhs, const apstring &rhs);
inline bool operator != (const apstring &lhs, const apstring &rhs);

std::ostream& operator << (std::ostream &os, const apstring &rhs);
std::istream& operator >> (std::istream &os, apstring &rhs);
void getline(std::istream &is, apstring &target);

#endif
