#include "page.h"
#include "Piglat.h"

using namespace std;

void Page::Build(char *workingfile)
{
    infile.open(workingfile, ios::in);

    if(!infile.is_open())
    {
        ::ShowUsage();
        cout << "  Couldn't open file: \"" << workingfile << '"' << endl;
        exit(1);
    }//if

    while(!infile.eof())
    {
        mylines.push_back(Line(this));
        mylines.rbegin()->Build();
    }//while
    infile.close();
}

void Page::Convert()
{
    for(vector<Line>::iterator cur = mylines.begin(); cur != mylines.end(); cur++)
        cur->Convert();
}

void Page::Display()
{
    for(vector<Line>::iterator cur = mylines.begin(); cur != mylines.end(); cur++)
        cur->Display();
}

int Page::TotalLines() const
{
    return mylines.size();
}

int Page::TotalWords() const
{
    int grandtotal = 0;
    for(vector<Line>::const_iterator cur = mylines.begin(); cur != mylines.end(); cur++)
        grandtotal += cur->TotalWords();

    return grandtotal;
}


int Page::TotalFirstVowels() const
{
    int grandtotal = 0;
    for(vector<Line>::const_iterator cur = mylines.begin(); cur != mylines.end(); cur++)
        grandtotal += cur->CountLeadingVowels();

    return grandtotal;
}

int Page::TotalNonleadingVowels() const
{
    int grandtotal = 0;
    for(vector<Line>::const_iterator cur = mylines.begin(); cur != mylines.end(); cur++)
        grandtotal += cur->CountNonleadingVowels();

    return grandtotal;
}
