#========================================================================
#  Borland Makefile -- created using AutoBake 1.06
#
#  Project:     Piglat
#  TimeStamp:   Mon Sep 13 18:00:58 2004
#========================================================================

#Piglat Macro Definitions
# Build Types:
#   DebugBuild   -- Debug Compile
#   ReleaseBuild -- Release Compile
# Project Types:
#   Con -- Console,          Win -- Windows
#   DLL -- Dynamic Link Lib, SLIB -- Static Lib
# Compiling Options:
#   RT -- dynamic runtime,   MT -- multi-threaded
#   WIDE -- Unicode/Wide
# API & Framework presets:
#   SDL -- SDL presets,      wxWindows -- wxWindows presets
Con = 1
DebugBuild = 1
args = ..\..\..\SDL-1.2\TODO

#Project Properties
Project     = Piglat
ProjectPath = C:\Borland\workspace\Piglat
BuildOutput = Piglat
ProjCFlags  = -DUSE_MYSTRING=1
ProjLFlags  = -t
ProjRFlags  =
ProjLib     =

OBJ = \
	Piglat.obj\
	Word.obj\
	page.obj\
	line.obj\
	PigAnalysis.obj\
	apstring.obj

RES =

#Borland Makefile Directives
.autodepend
.nosilent
.path.obj = C:\Borland\workspace\Piglat\Bin
.path.cpp = C:\Borland\workspace\Piglat
.path.exe = $(.path.obj)
.path.dll = $(.path.obj)
.path.lib = $(.path.obj)
.path.asm = $(.path.obj)
.path.res = $(.path.obj)

!include $(MAKEDIR)\..\bcc32proj.mak
