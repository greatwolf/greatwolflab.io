#ifndef PAGE_H_
#define PAGE_H_
#include <fstream>
#include <vector>
#include "line.h"

class Page
{
    public:
        void Build(char *workingfile);
        void Convert();
        void Display();
        int TotalLines() const;
        int TotalWords() const;
        int TotalFirstVowels() const;
        int TotalNonleadingVowels() const;

    private:
        friend Line;
        std::vector<Line> mylines;
        std::ifstream infile;
};

#endif
