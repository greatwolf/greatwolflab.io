#ifndef WORD_H_
#define WORD_H_
#ifndef USE_MYSTRING
    #include <string>
    typedef std::string apstring;
#else
    #include "apstring.h"
#endif

class Word;
extern const Word vowels;
extern const Word consonants;

class Word : public apstring
{
    public:
        Word(const char *rhs = "") : apstring(rhs) {}
        Word(const apstring &rhs) : apstring(rhs) {}
        inline bool VowelFirst() const
        {
            return (vowels.find((*this)[0]) != apstring::npos);
        }
        bool HasVowel() const;

        using apstring::operator =;
};

#endif
