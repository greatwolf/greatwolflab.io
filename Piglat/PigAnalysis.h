#ifndef PIGANALYSIS_H_
#define PIGANALYSIS_H_
#include "AnalysisObj.h"
#include "Word.h"

class PigAnalysis : public AnalysisObj<Word>
{
    public:
        PigAnalysis() : foundvowel(false), imdone(false) {}
        virtual void operator () (const char letter);
        virtual Word result();
        virtual bool finished() const;
        virtual Word handleEmptyWord();
        virtual void resetState();

        typedef Word ResultType;
    private:
        bool isYword() const;

        bool foundvowel;
        bool imdone ;
        Word modifiedWord, consonant, nonalpha;
};

#endif
